﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Net;
using System.Security.Cryptography;
using System.IO;
namespace SystemInfo
{
    class Info
    {
        static void Main(string[] args)
        {
            string enc_cpu = Encrypt(GetProcessorId());
            string enc_wguid = Encrypt(GetWindowsGUID());
            string enc_hdd = Encrypt(GetHDDSerialNo());
            WriteToFile(enc_cpu, enc_wguid, enc_hdd, "file.txt");
            
            Encrypt("file.txt", "noah.license","noahapp");
            File.Delete("file.txt");
        }

        private static void WriteToFile(string enc_cpu,string enc_wguid,string enc_hdd,string fileOutputName)
        {
            StreamWriter file = new StreamWriter(fileOutputName);
            file.WriteLine(enc_cpu);
            file.WriteLine(enc_wguid);
            file.WriteLine(enc_hdd);
            file.Close();
        }
        public static void Encrypt(string fileIn,
                string fileOut, string password)
        {
            FileStream fsIn = new FileStream(fileIn,
                FileMode.Open, FileAccess.Read);
            FileStream fsOut = new FileStream(fileOut,
                FileMode.OpenOrCreate, FileAccess.Write);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(GetProcessorId(), System.Text.Encoding.UTF8.GetBytes(GetHDDSerialNo()));
            Rijndael alg = Rijndael.Create();
            //alg.Padding = PaddingMode.PKCS7;
            alg.Key = pdb.GetBytes(32);
            alg.IV = pdb.GetBytes(16);

            CryptoStream cs = new CryptoStream(fsOut,
                alg.CreateEncryptor(), CryptoStreamMode.Write);


            int bufferLen = 4096;
            byte[] buffer = new byte[bufferLen];
            int bytesRead;
            do
            {
                bytesRead = fsIn.Read(buffer, 0, bufferLen);
                cs.Write(buffer, 0, bytesRead);
            } while (bytesRead != 0);
            //cs.FlushFinalBlock();
            cs.Close();
            fsIn.Close();
        } 

        private static string Encrypt(string str)
        {
            byte[] clearBytes =
              System.Text.Encoding.Unicode.GetBytes(str);

            PasswordDeriveBytes pdb = new PasswordDeriveBytes(GetProcessorId(),
                            new byte[] {0x32, 0x64, 0x78, 0x8b, 0x1c, 0x57, 0x5e,
            0x68, 0x59, 0x47, 0x4a, 0x73, 0x83});
       

            byte[] encryptedData = AE5_Encrypt(clearBytes,
                     pdb.GetBytes(32), pdb.GetBytes(16));


            return Convert.ToBase64String(encryptedData);
        }

        private static byte[] AE5_Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();

            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;

            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(clearData, 0, clearData.Length);

            cs.Close();

            byte[] encryptedData = ms.ToArray();
            return encryptedData;
        }

        public static String GetProcessorId()
        {

            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();
            String Id = String.Empty;
            foreach (ManagementObject mo in moc)
            {

                Id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return Id;

        }

        public static String GetHDDSerialNo()
        {
            ManagementClass mangnmt = new ManagementClass("Win32_LogicalDisk");
            ManagementObjectCollection mcol = mangnmt.GetInstances();
            string result = "";
            foreach (ManagementObject strt in mcol)
            {
                result += Convert.ToString(strt["VolumeSerialNumber"]);
            }
            return result;
        }

        public static String GetWindowsGUID()
        {
            ManagementClass mangnmt = new ManagementClass("Win32_OperatingSystem");
            ManagementObjectCollection mcol = mangnmt.GetInstances();
            string result = "";
            foreach (ManagementObject strt in mcol)
            {
                result += Convert.ToString(strt["SerialNumber"]);
            }
            return result;
        }
    }
}
